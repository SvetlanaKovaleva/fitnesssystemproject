CREATE DATABASE fitnesssystem;
USE fitnesssystem;
CREATE TABLE fitnesssystem.products (
  id INT(11) NOT NULL AUTO_INCREMENT,
  name VARCHAR(20) NOT NULL,
  kcal INT(11) NOT NULL,
  proteins INT(11) NOT NULL,
  fats INT(11) NOT NULL,
  carbohydrates INT(11) NOT NULL,
  PRIMARY KEY (id)
)
ENGINE=InnoDB
AUTO_INCREMENT=1
CHARACTER SET utf8 COLLATE utf8_general_ci;

CREATE TABLE fitnesssystem.users (
  id INT(11) NOT NULL AUTO_INCREMENT,
  firstName VARCHAR(45) NOT NULL,
  lastName VARCHAR(45) NOT NULL,
  age INT(11) NOT NULL,
  eMail VARCHAR(45) NOT NULL,
  phone VARCHAR(45) NOT NULL,
  login VARCHAR(45) NOT NULL,
  password VARCHAR(45) NOT NULL,
  role VARCHAR(20) NOT NULL,
  enabled TINYINT(1) NOT NULL default '1',
  PRIMARY KEY (id)
)
ENGINE=InnoDB
AUTO_INCREMENT=1
CHARACTER SET utf8 COLLATE utf8_general_ci;

CREATE TABLE fitnesssystem.userproducts (
  id_userPoducts INT(11) NOT NULL AUTO_INCREMENT,
  id_user INT(11) NOT NULL,
  id_product INT(11) NOT NULL,
  PRIMARY KEY (id_userPoducts),
  INDEX FK_cvvpqny80voqf8yps9ty5bghw USING BTREE (id_product),
  INDEX FK_rs195ykxoqf4ma9ksvk04dtg4 USING BTREE (id_user),
  CONSTRAINT FK_cvvpqny80voqf8yps9ty5bghw FOREIGN KEY (id_product)
    REFERENCES products(id),
  CONSTRAINT FK_rs195ykxoqf4ma9ksvk04dtg4 FOREIGN KEY (id_user)
    REFERENCES users(id)
)
ENGINE=InnoDB
AUTO_INCREMENT=1
CHARACTER SET utf8 COLLATE utf8_general_ci;

CREATE TABLE fitnesssystem.weightday (
  id_userPoducts INT(11) NOT NULL AUTO_INCREMENT,
  weight INT(11) NOT NULL,
  day VARCHAR(20) NOT NULL,
  PRIMARY KEY (id_userPoducts)
)
ENGINE=InnoDB
AUTO_INCREMENT=1
CHARACTER SET utf8 COLLATE utf8_general_ci;

INSERT INTO fitnesssystem.users (firstName,lastName, age, eMail, phone, login, password, role) VALUES(
'Svetlana', 'Kovaleva', 32, 's.kovaleva02081984@gmail.com', '80979065712', 'Admin', 'admin123', 'ROLE_ADMIN');
INSERT INTO fitnesssystem.users (firstName,lastName, age, eMail, phone, login, password, role) VALUES(
'FirstNameUser1', 'LastNameUser1', 25, 'user1@gmail.com', '80911111111', 'User1', '111111', 'ROLE_USER');
INSERT INTO fitnesssystem.users (firstName,lastName, age, eMail, phone, login, password, role) VALUES(
'FirstNameUser2', 'LastNameUser2', 30, 'user2@gmail.com', '80922222222', 'User2', '222222', 'ROLE_USER');
INSERT INTO fitnesssystem.users (firstName,lastName, age, eMail, phone, login, password, role) VALUES(
'FirstNameUser3', 'LastNameUser3', 33, 'user3@gmail.com', '80933333333', 'User3', '333333', 'ROLE_USER');
INSERT INTO fitnesssystem.users (firstName,lastName, age, eMail, phone, login, password, role) VALUES(
'Master', 'Master', 33, 'master@gmail.com', '80999999999', 'Master', 'master123', 'ROLE_MASTER');

INSERT INTO fitnesssystem.products (name,kcal, proteins, fats, carbohydrates) VALUES(
'apples', 60, 1, 1, 13);
INSERT INTO fitnesssystem.products (name,kcal, proteins, fats, carbohydrates) VALUES(
'apricots', 45, 1, 0, 10);
INSERT INTO fitnesssystem.products (name,kcal, proteins, fats, carbohydrates) VALUES(
'avocado', 197, 2, 19, 7);
INSERT INTO fitnesssystem.products (name,kcal, proteins, fats, carbohydrates) VALUES(
'bacon', 469, 15, 45, 2);
INSERT INTO fitnesssystem.products (name,kcal, proteins, fats, carbohydrates) VALUES(
'banana', 97, 3, 3, 22);
INSERT INTO fitnesssystem.products (name,kcal, proteins, fats, carbohydrates) VALUES(
'cashew', 607, 21, 48, 20);
INSERT INTO fitnesssystem.products (name,kcal, proteins, fats, carbohydrates) VALUES(
'cherry', 54, 1, 0, 11);
INSERT INTO fitnesssystem.products (name,kcal, proteins, fats, carbohydrates) VALUES(
'cocoa', 234, 14, 9, 26);
INSERT INTO fitnesssystem.products (name,kcal, proteins, fats, carbohydrates) VALUES(
'cucumbers', 19, 1, 0, 4);
INSERT INTO fitnesssystem.products (name,kcal, proteins, fats, carbohydrates) VALUES(
'eggs', 162, 15, 13, 1);
INSERT INTO fitnesssystem.products (name,kcal, proteins, fats, carbohydrates) VALUES(
'grapefruit', 41, 1, 0, 9);
INSERT INTO fitnesssystem.products (name,kcal, proteins, fats, carbohydrates) VALUES(
'honey', 315, 1, 0, 80);
INSERT INTO fitnesssystem.products (name,kcal, proteins, fats, carbohydrates) VALUES(
'kiwi', 51, 1, 1, 10);
INSERT INTO fitnesssystem.products (name,kcal, proteins, fats, carbohydrates) VALUES(
'lemon', 27, 1, 0, 4);
INSERT INTO fitnesssystem.products (name,kcal, proteins, fats, carbohydrates) VALUES(
'mango', 62, 1, 0, 13);
INSERT INTO fitnesssystem.products (name,kcal, proteins, fats, carbohydrates) VALUES(
'milk', 66, 7, 3, 7);
INSERT INTO fitnesssystem.products (name,kcal, proteins, fats, carbohydrates) VALUES(
'pineapple', 50, 0, 0, 12);
INSERT INTO fitnesssystem.products (name,kcal, proteins, fats, carbohydrates) VALUES(
'potatoes', 84, 2, 1, 20);
INSERT INTO fitnesssystem.products (name,kcal, proteins, fats, carbohydrates) VALUES(
'rice', 331, 7, 1, 75);
INSERT INTO fitnesssystem.products (name,kcal, proteins, fats, carbohydrates) VALUES(
'white bread', 254, 8, 3, 54);
INSERT INTO fitnesssystem.products (name,kcal, proteins, fats, carbohydrates) VALUES(
'melon', 35, 1, 0, 8);

INSERT INTO fitnesssystem.userproducts (id_user, id_product) VALUES(
2, 1);
INSERT INTO fitnesssystem.userproducts (id_user, id_product) VALUES(
2, 2);
INSERT INTO fitnesssystem.userproducts (id_user, id_product) VALUES(
2, 3);
INSERT INTO fitnesssystem.userproducts (id_user, id_product) VALUES(
2, 4);
INSERT INTO fitnesssystem.userproducts (id_user, id_product) VALUES(
2, 5);
INSERT INTO fitnesssystem.userproducts (id_user, id_product) VALUES(
2, 6);
INSERT INTO fitnesssystem.userproducts (id_user, id_product) VALUES(
2, 7);
INSERT INTO fitnesssystem.userproducts (id_user, id_product) VALUES(
2, 8);
INSERT INTO fitnesssystem.userproducts (id_user, id_product) VALUES(
2, 9);
INSERT INTO fitnesssystem.userproducts (id_user, id_product) VALUES(
2, 10);
INSERT INTO fitnesssystem.userproducts (id_user, id_product) VALUES(
2, 11);
INSERT INTO fitnesssystem.userproducts (id_user, id_product) VALUES(
2, 12);
INSERT INTO fitnesssystem.userproducts (id_user, id_product) VALUES(
2, 13);
INSERT INTO fitnesssystem.userproducts (id_user, id_product) VALUES(
2, 14);
INSERT INTO fitnesssystem.userproducts (id_user, id_product) VALUES(
2, 15);
INSERT INTO fitnesssystem.userproducts (id_user, id_product) VALUES(
2, 16);
INSERT INTO fitnesssystem.userproducts (id_user, id_product) VALUES(
2, 17);
INSERT INTO fitnesssystem.userproducts (id_user, id_product) VALUES(
2, 18);
INSERT INTO fitnesssystem.userproducts (id_user, id_product) VALUES(
2, 19);
INSERT INTO fitnesssystem.userproducts (id_user, id_product) VALUES(
2, 20);
INSERT INTO fitnesssystem.userproducts (id_user, id_product) VALUES(
2, 21);
INSERT INTO fitnesssystem.userproducts (id_user, id_product) VALUES(
3, 1);
INSERT INTO fitnesssystem.userproducts (id_user, id_product) VALUES(
3, 2);
INSERT INTO fitnesssystem.userproducts (id_user, id_product) VALUES(
3, 3);
INSERT INTO fitnesssystem.userproducts (id_user, id_product) VALUES(
3, 4);
INSERT INTO fitnesssystem.userproducts (id_user, id_product) VALUES(
3, 5);
INSERT INTO fitnesssystem.userproducts (id_user, id_product) VALUES(
3, 6);
INSERT INTO fitnesssystem.userproducts (id_user, id_product) VALUES(
3, 7);
INSERT INTO fitnesssystem.userproducts (id_user, id_product) VALUES(
3, 8);
INSERT INTO fitnesssystem.userproducts (id_user, id_product) VALUES(
3, 9);
INSERT INTO fitnesssystem.userproducts (id_user, id_product) VALUES(
3, 10);
INSERT INTO fitnesssystem.userproducts (id_user, id_product) VALUES(
3, 11);
INSERT INTO fitnesssystem.userproducts (id_user, id_product) VALUES(
3, 12);
INSERT INTO fitnesssystem.userproducts (id_user, id_product) VALUES(
3, 13);
INSERT INTO fitnesssystem.userproducts (id_user, id_product) VALUES(
3, 14);

INSERT INTO fitnesssystem.weightday (id_userPoducts, weight, day) VALUES(
1, 100, 'Monday');
INSERT INTO fitnesssystem.weightday (id_userPoducts, weight, day) VALUES(
2, 200, 'Monday');
INSERT INTO fitnesssystem.weightday (id_userPoducts, weight, day) VALUES(
3, 80, 'Monday');
INSERT INTO fitnesssystem.weightday (id_userPoducts, weight, day) VALUES(
4, 220, 'Tuesday');
INSERT INTO fitnesssystem.weightday (id_userPoducts, weight, day) VALUES(
5, 100, 'Tuesday');
INSERT INTO fitnesssystem.weightday (id_userPoducts, weight, day) VALUES(
6, 95, 'Tuesday');
INSERT INTO fitnesssystem.weightday (id_userPoducts, weight, day) VALUES(
7, 120, 'Wednesday');
INSERT INTO fitnesssystem.weightday (id_userPoducts, weight, day) VALUES(
8, 200, 'Wednesday');
INSERT INTO fitnesssystem.weightday (id_userPoducts, weight, day) VALUES(
9,100, 'Wednesday');
INSERT INTO fitnesssystem.weightday (id_userPoducts, weight, day) VALUES(
10, 60, 'Thursday');
INSERT INTO fitnesssystem.weightday (id_userPoducts, weight, day) VALUES(
11, 200, 'Thursday');
INSERT INTO fitnesssystem.weightday (id_userPoducts, weight, day) VALUES(
12, 85, 'Thursday');
INSERT INTO fitnesssystem.weightday (id_userPoducts, weight, day) VALUES(
13, 95, 'Friday');
INSERT INTO fitnesssystem.weightday (id_userPoducts, weight, day) VALUES(
14, 90, 'Friday');
INSERT INTO fitnesssystem.weightday (id_userPoducts, weight, day) VALUES(
15, 190, 'Friday');
INSERT INTO fitnesssystem.weightday (id_userPoducts, weight, day) VALUES(
16, 120, 'Saturday');
INSERT INTO fitnesssystem.weightday (id_userPoducts, weight, day) VALUES(
17, 50, 'Saturday');
INSERT INTO fitnesssystem.weightday (id_userPoducts, weight, day) VALUES(
18, 111, 'Saturday');
INSERT INTO fitnesssystem.weightday (id_userPoducts, weight, day) VALUES(
19, 40, 'Sunday');
INSERT INTO fitnesssystem.weightday (id_userPoducts, weight, day) VALUES(
20, 140, 'Sunday');
INSERT INTO fitnesssystem.weightday (id_userPoducts, weight, day) VALUES(
21, 95, 'Sunday');
INSERT INTO fitnesssystem.weightday (id_userPoducts, weight, day) VALUES(
22, 100, 'Monday');
INSERT INTO fitnesssystem.weightday (id_userPoducts, weight, day) VALUES(
23, 140, 'Monday');
INSERT INTO fitnesssystem.weightday (id_userPoducts, weight, day) VALUES(
24, 80, 'Tuesday');
INSERT INTO fitnesssystem.weightday (id_userPoducts, weight, day) VALUES(
25, 100, 'Tuesday');
INSERT INTO fitnesssystem.weightday (id_userPoducts, weight, day) VALUES(
26, 115, 'Wednesday');
INSERT INTO fitnesssystem.weightday (id_userPoducts, weight, day) VALUES(
27, 50, 'Wednesday');
INSERT INTO fitnesssystem.weightday (id_userPoducts, weight, day) VALUES(
28, 90, 'Thursday');
INSERT INTO fitnesssystem.weightday (id_userPoducts, weight, day) VALUES(
29, 100, 'Thursday');
INSERT INTO fitnesssystem.weightday (id_userPoducts, weight, day) VALUES(
30, 110, 'Friday');
INSERT INTO fitnesssystem.weightday (id_userPoducts, weight, day) VALUES(
31, 90, 'Friday');
INSERT INTO fitnesssystem.weightday (id_userPoducts, weight, day) VALUES(
32, 100, 'Saturday');
INSERT INTO fitnesssystem.weightday (id_userPoducts, weight, day) VALUES(
33, 200, 'Saturday');
INSERT INTO fitnesssystem.weightday (id_userPoducts, weight, day) VALUES(
34, 100, 'Sunday');
INSERT INTO fitnesssystem.weightday (id_userPoducts, weight, day) VALUES(
35, 100, 'Sunday');


