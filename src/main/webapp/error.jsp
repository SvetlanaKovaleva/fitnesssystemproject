<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>FitnessSystem</title>
	<script type="text/javascript">
	function backToWelcome() {
		document.getElementById("backToWelcome").submit();
	}
	</script>
</head>
<body>
	<h1>HTTP Status 403 - Access is denied. You don't have permission to access this page!</h1>
	<form action="backToMainMenuFitnessSystem" id = "backToWelcome">
	</form>
	<a href = "javascript:backToWelcome();">Back to main menu</a>
</body>
</html>