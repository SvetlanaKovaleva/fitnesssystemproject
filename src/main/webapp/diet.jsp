<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>FitnessSystem</title>
	<link href="<c:url value="/resources/css/removeProduct.css" />" type="text/css" rel="stylesheet"/>
	<script type="text/javascript">
		function backToWelcome (){
			document.getElementById("backToWelcome").submit();
		}
	</script>
</head>
<body>
	<c:set var = "listAllProducts" value="${listAllProducts}"/>
	<c:set var = "listResultUserProducts" value="${listResultUserProducts}"/>
	<c:set var = "dayTotal" value="${dayTotal}"/>
	<c:set var = "login" value="${login}"/>
	<c:set var = "userProducts" value="${userProducts}"/>
	<c:set var = "dietUser" value = "${DietUser}"/>
	<form action="backToWelcomeFitnessSystem" method = "post" id="backToWelcome">
	</form>
	<p align="center"><a href = "javascript: backToWelcome()" >Back to main menu</a></p>
	<div id="form">
		<h1 >Select product, weight, day for <c:out value="${login}"/> </h1>
		<form action="saveDietInDBFitnessSystem" method="post">
			<div class="row">
				<p><label for="nameProduct">Name of product:</label></p>
				<select name="idProducts" size="3" style="width:155px" id="idProducts">
					<c:forEach var = "listAllProducts" items="${listAllProducts}">
						<option value="<c:out value="${listAllProducts.id}"/>"><c:out value="${listAllProducts.name}"/> </option>
					</c:forEach>
				</select><br />
			</div>
			<div class="row">
				<p><label for="weight">Weight,g:</label></p>
				<input type="text" name = "weight" maxlength="8" id="weight" style="width:145px"/>
			</div>
			<div class="row">
				<p><label for="day">Day of a week:</label></p>
				<select name="day" size="3" style="width:155px" id="day">
					<c:forEach var = "dayTotal" items="${dayTotal}">
						<option> <c:out value="${dayTotal.day}"/> </option>
					</c:forEach>
				</select><br />
			</div>
			<input type="submit" name="select" value="Select">
			<input type="reset" name="reset" value="Reset">
		</form>
	</div>
	<div id="content">
		<div id="top-line"></div>
			<form action = "deleteDietInDBFitnessSystem" method = "post">
				<table>
					<caption>Diet on the week</caption>
					<thead>
						<tr>
							<th>Delete</th>
							<th>Product</th>
							<th>Weight, g.</th>
							<th>kCal</th>
							<th>Proteins</th>
							<th>Fats</th>
							<th>Carbohydrates</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="dayTotal" items="${dayTotal}">
							<tr> 
								<td colspan="7" align="center"> <strong><c:out value="${dayTotal.day}"/></strong></td>
							</tr>
							<c:forEach var = "listResultUserProducts" items="${listResultUserProducts}">
								<c:if test="${dayTotal.day == listResultUserProducts.dayOfAWeek}">
									<tr align="center">	 
										<td><input type = "checkbox" name = "<c:out value="${listResultUserProducts.id_userPoducts}"/>"></td>				
										<td><c:out value="${listResultUserProducts.nameProduct}"/></td>
										<td><c:out value="${listResultUserProducts.weight}"/></td>
										<td><c:out value="${listResultUserProducts.kcal}"/></td>
										<td><c:out value="${listResultUserProducts.proteins}"/></td>
										<td><c:out value="${listResultUserProducts.fats}"/></td>
										<td><c:out value="${listResultUserProducts.carbohydrates}"/></td>
									</tr>
								</c:if>
							</c:forEach>
							<tr align="center">					
								<td colspan="3"><strong>Total:</strong></td>
								<td><strong><c:out value="${dayTotal.kCalTotal}"/></strong></td>
								<td><strong><c:out value="${dayTotal.proteinsTotal}"/></strong></td>
								<td><strong><c:out value="${dayTotal.fatsTotal}"/></strong></td>
								<td><strong><c:out value="${dayTotal.carbohydratesTotal}"/></strong></td>
							</tr>
						</c:forEach>
					</tbody>
			   </table>
			   <input type = "submit" name="submit" value = "Delete">
			   <input type = "reset" name="submit" value = "Reset">
		   </form>
	</div>			
</body>
</html>