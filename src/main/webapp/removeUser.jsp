<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>FitnessSystem</title>
	<link href="<c:url value="/resources/css/removeUsers.css" />" type="text/css" rel="stylesheet"/>
	<script type="text/javascript">
	function backToWelcome() {
		document.getElementById("backToWelcome").submit();
	}
	</script>
</head>
<body>
	<c:set var="listAllUsers" value = "${listAllUsers}"/>
	<form action="backToMainMenuFitnessSystem" id = "backToWelcome">
	</form>
	<p align="center"><a href = "javascript:backToWelcome();" >Back to main menu</a></p>
	<div id="content">
		<form action = "deleteUserFitnessSystem" method = "post">
			<h1>Delete of users</h1>
			<table>
				<caption>Table of users</caption>
					<thead>
						<tr>
							<th>Delete</th>
							<th>First name</th>
							<th>Last name</th>
							<th>Age</th>
							<th>E-mail</th>
							<th>Phone</th>
							<th>Login</th>
							<th>Password</th>
							<th>Role</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="listAllUsers" items="${listAllUsers}">
							<c:if test="${listAllUsers.role!='Admin'}"/> 
								<tr> 
									<td><input type="checkbox" name = "<c:out value="${listAllUsers.id}"/>"></td>
									<td> <c:out value="${listAllUsers.firstName}"/> </td>
									<td><c:out value="${listAllUsers.lastName}"/></td>
									<td><c:out value="${listAllUsers.age}"/></td>
									<td><c:out value="${listAllUsers.eMail}"/></td>
									<td><c:out value="${listAllUsers.phone}"/></td>
									<td><c:out value="${listAllUsers.login}"/></td>
									<td><c:out value="${listAllUsers.password}"/></td>
									<td><c:out value="${listAllUsers.role}"/></td>
								</tr>
						</c:forEach>
					</tbody>
			</table>
			<input type = "submit" name="submit" value = "Delete">
			<input type = "reset" name="submit" value = "Reset">
		</form>
	</div>
</body>
</html>