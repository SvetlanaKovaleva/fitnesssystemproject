<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>FitnessSystem</title>
	<link href="<c:url value="/resources/css/addProducts.css" />" type="text/css" rel="stylesheet"/>
	<script type="text/javascript">
		function backToWelcome() {
			document.getElementById("backToWelcome").submit();
		}
	</script>
</head>
<body>
	<c:set var="alertOk" value="${alertOk}"/>
	<c:set var = "listAllProducts" value = "${listAllProducts}"/>
	<form action="backToMainMenuFitnessSystem" id = "backToWelcome">
	</form>
	<p align="center"><a href = "javascript:backToWelcome();">Back to Main menu</a></p>
	<div id="form">
	<h1>Add product</h1>
		<c:if test="${errorEmptyFilds!=null}">
			<span style="color: red;"><b> <c:out value="${errorEmptyFilds}"/> </b></span>
		</c:if>
	<form action="saveProductInDBFitnessSystem" method="post">
		<div class="row">
			<p><label for = "name">Name of product:</label></p>
			<input type = "text" name = "name" id = "name">
			<c:if test="${errorNameProduct!=null}">
				<span style="color: red;"><b> <c:out value="${errorNameProduct}"/> </b></span>
			</c:if>
		</div>
		<div class="row">
			<p><label for = "kcal">kCal (in 100g. of product):</label></p>
			<input type = "text" name = "kcal" id = "kcal">
			<c:if test="${errorNotCorrectFilds!=null}">
				<span style="color: red;"><b> <c:out value="${errorNotCorrectFilds}"/> </b></span>
			</c:if>
		</div>
		<div class="row">
			<p><label for = "proteins">Proteins,g (in 100g. of product):</label></p>
			<input type = "text" name = "proteins" id = "proteins">
			<c:if test="${errorNotCorrectFilds!=null}">
				<span style="color: red;"><b> <c:out value="${errorNotCorrectFilds}"/> </b></span>
			</c:if>
		</div>
		<div class="row">
			<p><label for = "fats">Fats,g (in 100g. of product:)</label></p>
			<input type = "text" name = "fats" id = "fats">
			<c:if test="${errorNotCorrectFilds!=null}">
				<span style="color: red;"><b> <c:out value="${errorNotCorrectFilds}"/> </b></span>
			</c:if>
		</div>
		<div class="row">
			<p><label for = "carbohydrates">Carbohydrates,g (in 100g. of product):</label></p>
			<input type = "text" name = "carbohydrates" id = "carbohydrates">
			<c:if test="${errorNotCorrectFilds!=null}">
				<span style="color: red;"><b> <c:out value="${errorNotCorrectFilds}"/> </b></span>
			</c:if>
		</div>
		<input type = "submit" name = "submit" value = "Add of product">
		<input type = "reset" name = "reset" value = "Reset">
	</form> 
	</div>
	<div id="content">
		<table>
			<caption>Table of products</caption>
				<thead>
					<tr>
						<th>Name</th>
						<th>kCal (in 100g of product)</th>
						<th>Proteins,g (in 100g of product)</th>
						<th>Fats,g (in 100g of product)</th>
						<th>Carbohydrates,g (in 100g of product)</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="listAllProducts" items="${listAllProducts}">
						<tr id ="u<c:out value="${listAllProducts}"/>">
							<td> <c:out value="${listAllProducts.name}"/> </td>
							<td><c:out value="${listAllProducts.kcal}"/></td>
							<td><c:out value="${listAllProducts.proteins}"/></td>
							<td><c:out value="${listAllProducts.fats}"/></td>
							<td><c:out value="${listAllProducts.carbohydrates}"/></td>
						</tr>
					</c:forEach>
				</tbody>
		</table>
	</div>
</body>
</html>