<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>FitnessSystem</title>
	<link href="<c:url value="/resources/css/showUsers.css" />" type="text/css" rel="stylesheet"/>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script>
		$(document).ready(function(){
		
			  $("tbody tr").click(function() {
				var tdArray = $(this).find("td");
				var id = tdArray[0].innerHTML;
			 	var firstName = tdArray[1].innerHTML;
				var lastName = tdArray[2].innerHTML;
				var age = tdArray[3].innerHTML;
				var eMail = tdArray[4].innerHTML;
				var phone = tdArray[5].innerHTML;
				var login = tdArray[6].innerHTML;
				var password = tdArray[7].innerHTML;
				var role = tdArray[8].innerHTML;
				
				$("#id").val(id);
				$("#firstName").val(firstName);
				$("#lastName").val(lastName);
				$("#age").val(age);
				$("#eMail").val(eMail);
				$("#phone").val(phone);
				$("#login").val(login);
				$("#password").val(password);
				$("#role").val(role);
			
			 });
		
			}); 
		function backToWelcome() {
			document.getElementById("backToWelcome").submit();
		}
	</script>
</head>
<body>
	<c:set var="listAllUsers" value="${listAllUsers}"/>
	<form action="backToMainMenuFitnessSystem" id = "backToWelcome">
	</form>
	<p align="center"><a href = "javascript:backToWelcome();">Back to main menu</a></p>
	<div id="content">
		<h1>Show and edit of users</h1>
		<table>
			<caption>Table of users</caption>
				<thead>
					<tr>
						<th>id</th>
						<th>First name</th>
						<th>Last name</th>
						<th>Age</th>
						<th>E-mail</th>
						<th>Phone</th>
						<th>Login</th>
						<th>Password</th>
						<th>Role</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var = "listAllUsers" items="${listAllUsers}" > 
						<tr id ="u<c:out value="${listAllUsers}"/>"> 
							<td><c:out value="${listAllUsers.id}"/></td>
							<td><c:out value="${listAllUsers.firstName}"/></td>
							<td><c:out value="${listAllUsers.lastName}"/></td>
							<td><c:out value="${listAllUsers.age}"/></td>
							<td><c:out value="${listAllUsers.eMail}"/></td>
							<td><c:out value="${listAllUsers.phone}"/></td>
							<td><c:out value="${listAllUsers.login}"/></td>
							<td><c:out value="${listAllUsers.password}"/></td>
							<td><c:out value="${listAllUsers.role}"/></td>
						</tr>
					</c:forEach>
				</tbody>
		</table>
		<form action="editUserFitnessSystem" method="post">
			<input type="hidden" name="id" id="id">
			<c:if test="${errorEmpty!=null}">
				<span style="color: red;"><b> <c:out value="${errorEmpty}"/> </b></span>
			</c:if>
			<div class="row">
				<p><label for="firstName">First name:</label></p>
				<input type="text" name="firstName" id="firstName">
			</div>
			<div class="row">
				<p><label for="lastName">Last name:</label></p>
				<input type="text" name="lastName" id="lastName">
			</div>
			<div class="row">
				<p><label for="age">Age:</label></p>
				<input type="text" name="age" id="age">
				<c:if test="${errorAge!=null}">
					<span style="color: red;"><b> <c:out value="${errorAge}"/> </b></span>
				</c:if>
			</div>
			<div class="row">
				<p><label for="eMail">E-mail:</label></p>
				<input type="text" name="eMail" id="eMail">
				<c:if test="${errorEMail!=null}">
					<span style="color: red;"><b> <c:out value="${errorEMail}"/> </b></span>
				</c:if>
			</div>
			<div class="row">
				<p><label for="phone">Phone:</label></p>
				<input type="text" name="phone" id="phone">
			</div>
			<div class="row">
				<p><label for="login">Login:</label></p>
				<input type="text" name="login" id="login">
				<c:if test="${errorLoginExist!=null}">
					<span style="color: red;"><b> <c:out value="${errorLoginExist}"></c:out> </b></span>
				</c:if>
			</div>
			<div class="row">
				<p><label for="password">Password:</label></p>
				<input type="text" name="password" id="password">
			</div>
			<div class="row">
				<p><label for="role">Role:</label></p>
				<input type="text" name="role" id="role"><br />
			</div>
			<input type="submit" value="Save">
		</form>
	</div>
</body>
</html>