<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>FitnessSystem</title>
	<link href="<c:url value="/resources/css/removeProduct.css" />" type="text/css" rel="stylesheet"/>
	<script type="text/javascript">
		function checkSelection(){
			var selIndex = document.getElementById("login").selectedIndex;
			if(selIndex!=-1){
				document.getElementById("goToDiet").disabled = false;
			}
		}
		function backToWelcome() {
			document.getElementById("backToWelcome").submit();
		}
	</script>
</head>
<body>
	<c:set var = "listAllUsers" value = "${listAllUsers}"/>
	<form action="backToMainMenuFitnessSystem" id = "backToWelcome">
	</form>
	<p align="center"><a href = "javascript:backToWelcome();" >Back to main menu</a></p>
	<div id="form">
		<h1 >Select of user</h1>
		<c:if test="${errorLogin!=null}">
			<span style="color: red;"><b> <c:out value="${errorLogin}"/> </b></span>
		</c:if>
		<form action="selectUserForCreateTableDiet" method="post">
			<select name="loginUserList" size="3" style="width:155px" id = "login" onchange="checkSelection()" >
				<c:forEach var="listAllUsers" items="${listAllUsers}">
					<c:if test="${listAllUsers.role!='ROLE_ADMIN' && listAllUsers.role!='ROLE_MASTER'}">
						<option value="<c:out value="${listAllUsers.id}"/>"><c:out value="${listAllUsers.login}"/> </option>
					</c:if>
				</c:forEach>
			</select><br />
			<input type="submit" name="submit" disabled="disabled" id="goToDiet" value="Select of user">
		</form>
	</div>
</body>
</html>