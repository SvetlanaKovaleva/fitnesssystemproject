<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>FitnessSystem</title>
	<link href="<c:url value="/resources/css/formRegistration.css" />" type="text/css" rel="stylesheet"/>
</head>
<body>
	<div id="form">
		<h1>Registration form</h1>
		<form action="regestrationFitnessSystem" method="post">
			<c:if test="${errorEmpty!=null}">
				<span style="color: red;"><b> <c:out value="${errorEmpty}"/></b></span>
			</c:if>
			<div class="row">
				<p><label for="login">Login:</label></p>
				<input type="text" name="loginNew" maxlength="20" id="login"/>
				<c:if test="${errorLogin!=null}">
					<span style="color: red;"><b><c:out value="${errorLogin}"/></b></span>
				</c:if>
			</div>
			<div class="row">
				<p><label for="password">Password:</label></p>
				<input type="password" name="passwordNew" maxlength="20" id="password"><br/>
			</div>
			<div class="row">
				<p><label for="password2">Repeat password:</label></p>
				<input type="password" name="password2" maxlength="20" id="password2">
				<c:if test="${errorPassword!=null}">
					<span style="color: red;"><b><c:out value="${errorPassword}"/></b></span>
				</c:if>
			</div>
			<div class="row">
				<p><label for="age">Age:</label></p>
				<input type="text" name="ageNew" maxlength="3" id="age">
				<c:if test="${errorAge!=null}">
					<span style="color: red;"><b><c:out value="${errorAge}"/></b></span>
				</c:if>
			</div>
			<div class="row">
				<p><label for="firstName">First name:</label></p>
				<input type="text" name="firstName" maxlength="20" id="firstName">
			</div>
			<div class="row">
				<p><label for="lastName">Last name:</label></p>
				<input type="text" name="lastName" maxlength="20" id="lastName">
			</div>
			<div class="row">
				<p><label for="eMail">E-mail:</label></p>
				<input type="text" name="eMail" maxlength="40" id="eMail">
				<c:if test="${errorEMail!=null}">
					<span style="color: red;"><b> <c:out value="${errorEMail}"/> </b></span>
				</c:if>
				<c:if test="${errorUniqEMail!=null}">
					<span style="color: red;"><b> <c:out value="${errorUniqEMail}"/> </b></span>
				</c:if>
			</div>
			<div class="row">
				<p><label for="phone">Phone:</label></p>
				<input type="text" name="phone" maxlength="20" id="phone"><br/>
			</div>
			<input type="submit" name="registration" value="Registration">
			<input type="reset" name="reset" value="Reset">
		</form>
	</div>
</body>
</html>