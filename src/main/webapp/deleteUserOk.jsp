<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>FitnessSystem</title>
	<script type="text/javascript">
		function back() {
			location.href("welcome.jsp");
		}
	</script>
</head>
<body> 
	<h1>User <c:out value="${login}"/> has deleted</h1>
	<input type="button" name = "button" value="Back to main menu" onclick="back()">
</body>
</html>