<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>FitnessSystem</title>
	<link href="<c:url value="/resources/css/removeProduct.css" />" type="text/css" rel="stylesheet"/>
	<script type="text/javascript">
	function backToWelcome() {
		document.getElementById("backToWelcome").submit();
	}
	</script>
</head>
<body>
	<c:set var="listAllProducts" value = "${listAllProducts}"/>
	<form action="backToMainMenuFitnessSystem" id = "backToWelcome">
	</form>
	<p align="center"><a href = "javascript:backToWelcome();">Back to Main menu</a></p>
	<div id="content">
		<form action = "deleteProductFitnessSystem" method = "post">
		<h1>Delete of products</h1>
		<table>
			<caption>Table of products</caption>
				<thead>
					<tr>
						<th>Delete</th>
						<th>Name</th>
						<th>kCal (in 100g of product)</th>
						<th>Proteins,g (in 100g of product)</th>
						<th>Fats,g (in 100g of product)</th>
						<th>Carbohydrates,g (in 100g of product)</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="listAllProducts" items="${listAllProducts}">
						<tr> 
							<td><input type="checkbox" name = "<c:out value="${listAllProducts.id}"/>"  /></td>
							<td> <c:out value="${listAllProducts.name}"/> </td>
							<td><c:out value="${listAllProducts.kcal}"/></td>
							<td><c:out value="${listAllProducts.proteins}"/></td>
							<td><c:out value="${listAllProducts.fats}"/></td>
							<td><c:out value="${listAllProducts.carbohydrates}"/></td>
						</tr>
					</c:forEach>
				</tbody>
		</table>
			<input type = "submit" name="submit" value = "Delete">
			<input type = "reset" name="submit" value = "Reset">
		</form>
	</div>
</body>
</html>