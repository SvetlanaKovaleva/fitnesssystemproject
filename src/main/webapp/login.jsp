<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>FitnessSystem</title>
	<link href="<c:url value="/resources/css/formLogin.css" />" type="text/css" rel="stylesheet"/>
</head>
<body>
	<c:if test=""></c:if>
	<div id="form">
		<h1>Entrance</h1>
		<form action="<c:url value='/j_spring_security_check'/>"  method="post">
			<div class="row">
				<p><label for="login">Enter login:</label></p>
				<input type="text" name="username" maxlength="20" id="login"/>
			</div>
			<div class="row">
				<p><label for="password">Enter password:</label></p>
				<input type="password" name="userpassword" maxlength="20" id="password"><br/>
			</div>
			<input type="submit" name="submit" value="Ok">
			<input type="reset" name="reset" value="Reset">
		</form>
		<c:if test="${not empty error}">
			<div class="loginError">
				${error}
			</div>
		</c:if>
		<c:if test="${not empty logout}">
			<div class="logout">
				${logout}
			</div>
		</c:if>
	</div>
</body>
</html>