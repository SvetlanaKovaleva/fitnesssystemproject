<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 <%@page import="model.User"%>
 <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
 <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>FitnessSystem</title>
	<link href="<c:url value="/resources/css/welcome.css" />" type="text/css" rel="stylesheet"/>
</head>
<body>
	<c:set var="user" value="${DietUser}"/>
	<div id="content">
		<div id="top-line"></div> 
			<form action = "selectUserForCreateTableDiet" method = "post">
				<c:if test="${errorEmpty!=null}"> 
					<span style="color: red;"><b><c:out value="${errorEmpty}"/></b></span><br />
				</c:if> 
				<c:if test="${errorWeight!=null}"> 
					<span style="color: red;"><b><c:out value="${errorWeight}"/></b></span><br />
				</c:if> 
				<c:if test="${errorCheckProduct!=null}"> 
					<span style="color: red;"><b><c:out value="${errorCheckProduct}"/></b></span><br />
				</c:if> 
				<h1> Product has added.</h1>
				<input type="submit" name = "submit" value="Back to create of diet" >
			</form>
	</div>
</body>
</html>