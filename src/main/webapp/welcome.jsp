
<%@page import="org.apache.jasper.tagplugins.jstl.core.ForEach"%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>   
 <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>FitnessSystem</title>
	<link href="<c:url value="/resources/css/welcome.css" />" type="text/css" rel="stylesheet"/>
</head>
<body>
	<c:set var = "user" scope="session" value = "${User}" />
	<h1 align="center">Welcome, <c:out value="${UserName}"></c:out> !</h1>
		<c:if test="${user.role=='ROLE_USER'}">
			<c:set var ="arrayDayTotal" value="${dayTotal}"/>
			<c:set var = "listResultUserProducts" value = "${listResultUserProducts}"/>
			 <div id="content">
				<div id="top-line"></div>
				<table>
					<caption>Diet on the week</caption>
					<thead>
						<tr>
							<th>Product</th>
							<th>Weight, g.</th>
							<th>kCal</th>
							<th>Proteins</th>
							<th>Fats</th>
							<th>Carbohydrates</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="daytotal" items="${arrayDayTotal}">
							<tr>
								<td colspan="6" align="center"> <strong><c:out value="${daytotal.day}"/></strong></td>
							</tr>
							<c:forEach var = "resultUserProducts" items = "${listResultUserProducts}"> 
								<c:if test="${daytotal.day==resultUserProducts.dayOfAWeek}">
									<tr align="center">					
										<td> <c:out value="${resultUserProducts.nameProduct}"/> </td>
										<td><c:out value="${resultUserProducts.weight}"/></td>
										<td><c:out value="${resultUserProducts.kcal}"/></td>
										<td><c:out value="${resultUserProducts.proteins}"/></td>
										<td><c:out value="${resultUserProducts.fats}"/></td>
										<td><c:out value="${resultUserProducts.carbohydrates}"/></td>
									</tr>
								</c:if>
							</c:forEach>
							<tr align="center">					
								<td colspan="2"><strong>Total:</strong></td>
								<td><strong><c:out value="${daytotal.kCalTotal}"/></strong></td>
								<td><strong><c:out value="${daytotal.proteinsTotal}"/></strong></td>
								<td><strong><c:out value="${daytotal.fatsTotal}"/></strong></td>
								<td><strong><c:out value="${daytotal.carbohydratesTotal}"/></strong></td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
				<div class="form1">
					<form action = "<c:url value="/j_spring_security_logout"/>" method = "post">
		 				<input type="submit" name = "submit" value = "Exit">
					</form>	
				</div>		
			</div>
		</c:if> 
		<c:if test="${user.role!='ROLE_USER'}">
			<div id="form">
				<div class="form1">
					<form action="addUserFitnessSystem" method="post">
						<input type="submit" name="submit1" value="Add of user (for Admin)">			
					</form>
				</div>
				<div class="form1">
					<form action="showUsersFitnessSystem" method="post">
						<input type="submit" name="submit2" value="Show and edit of users (for Admin)">
					</form>	
				</div>
				<div class="form1">
					<form action="removeUserFitnessSystem" method="post">
						<input type="submit" name="submit3" value="Remove of user (for Admin)">
					</form>
				</div>
				<div class="form1">
					<form action="addProductFitnessSystem" method="post">
						<input type="submit" name="submit6" value="Show and add of products ">
					</form>
				</div>
				<div class="form1">
					<form action="removeProductFitnessSystem" method="post">
						<input type="submit" name="submit8" value="Remove of product ">
					</form>
				</div>
				<div class="form1">
					<form action="createTableDietFitnessSystem" method="post">
						<input type="submit" name="submit9" value="Create and edit of table diet">
					</form>
				</div>
				<div class="form1">
					<form action = "<c:url value="/j_spring_security_logout"/>" method = "post">
			 			<input type="submit" name = "submit" value = "Exit">
					</form>	
				</div>	
			</div>	
		</c:if>
</body>
</html>