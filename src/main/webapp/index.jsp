<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>FitnessSystem</title>
	<link href="<c:url value="/resources/css/style.css" />" type="text/css" rel="stylesheet"/>
	<script type="text/javascript">
		function start() {
		document.getElementById("start").submit();
		}
	</script>
</head>
<body>
	<div id="form">
		<h1>Fitness System</h1>
		<a href="registration.jsp">Registration</a><br />
		<a href="javascript:start();">Login</a>
		<form action="<c:url value='loginFitnessSystem'/>" method="post" id="start">
		</form>
	</div>
</body>
</html>