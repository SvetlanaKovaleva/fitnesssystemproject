package DAO;

import java.util.ArrayList;
import java.util.List;

import model.Product;
import model.User;
import model.UserProduct;
import model.WeightDay;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

public class DAOfitSystemImpl implements DAOfitSystem {

	Session session = null;
	Transaction transaction = null;
	Configuration configuration = new Configuration();
	ServiceRegistry serviceRegistry = null;
	SessionFactory sessionFactory = null;
	List<User> listUser = new ArrayList<User>();
	List<Product> listProduct = new ArrayList<Product>();

	public void createConfiguration() {
		if (sessionFactory == null) {
			configuration.configure("FitSysHib.cfg.xml");
			serviceRegistry = new ServiceRegistryBuilder().applySettings(configuration.getProperties()).buildServiceRegistry();
			sessionFactory = configuration.buildSessionFactory(serviceRegistry);
		}
	}

	public void openSession() {
		createConfiguration();
		session = sessionFactory.openSession();
		transaction = session.beginTransaction();
	}

	public void closeSession() {
		transaction.commit();
	}

	@Override
	public void addUserInDB(User user) {
		openSession();
		session.save(user);
		closeSession();
	}

	@Override
	public boolean checkUniquenessOfLogin(String login) {
		User user = getUserByLogin(login);
		if (user != null) {
			return false; // This login has already existed
		} else {
			return true;
		}
	}

	@Override
	public boolean checkUniquenessOfEmail(String eMail) {
		openSession();
		listUser.clear();
		listUser = session.createQuery("FROM User WHERE eMail=:eMail").setParameter("eMail", eMail).list();
		closeSession();
		if (listUser.size() != 0) {
			return false; // This eMail has already existed
		} else {
			return true;
		}
	}

	@Override
	public boolean checkLoginAndPassword(String login, String password) {
		openSession();
		listUser.clear();
		listUser = session.createQuery("FROM User WHERE login=:login AND password=:password").setParameter("login", login).setParameter("password", password).list();
		closeSession();
		if (listUser.size() != 0) {
			return true; // this login and password have existed
		} else {
			return false;
		}
	}

	@Override
	public Product getProduct(int idProduct) {
		openSession();
		Product product = (Product) session.load(Product.class, idProduct);
		closeSession();
		return product;
	}

	@Override
	public List<User> getAllUsers() {
		openSession();
		listUser.clear();
		listUser = session.createQuery("FROM User u").list();
		closeSession();
		return listUser;
	}

	@Override
	public User getUserByLogin(String login) {
		openSession();
		listUser = session.createQuery("FROM User WHERE login=:login").setParameter("login", login).list();
		closeSession();
		if (listUser.size() != 0) {
			return listUser.get(0);
		} else {
			return null;
		}
	}

	@Override
	public void deleteUser(int id) {
		openSession();
		User user = new User();
		user.setId(id);
		session.delete(user);
		session.flush();
		closeSession();
	}

	@Override
	public void saveChangeUser(User user) {
		openSession();
		User userNew = (User) session.load(User.class, user.getId());
		userNew.setFirstName(user.getFirstName());
		userNew.setLastName(user.getLastName());
		userNew.setAge(user.getAge());
		userNew.seteMail(user.geteMail());
		userNew.setLogin(user.getLogin());
		userNew.setPassword(user.getPassword());
		userNew.setPhone(user.getPhone());
		userNew.setRole(user.getRole());
		session.update(userNew);
		closeSession();
	}

	@Override
	public void addProduct(Product product) {
		openSession();
		session.save(product);
		closeSession();
	}

	@Override
	public boolean checkUniquenessOfNameProduct(String name) {
		openSession();
		listProduct.clear();
		listProduct = session.createQuery("FROM Product WHERE name=:name").setParameter("name", name).list();
		closeSession();
		if (listProduct.size() != 0) {
			return false; // This product has already existed
		} else {
			return true;
		}
	}

	@Override
	public List<Product> getAllProducts() {
		openSession();
		listProduct.clear();
		listProduct = session.createQuery("FROM Product p").list();
		closeSession();
		return listProduct;
	}

	@Override
	public void deleteProduct(int id) {
		openSession();
		Product product = new Product();
		product.setId(id);
		session.delete(product);
		session.flush();
		closeSession();
	}

	@Override
	public User getUserById(int id) {
		openSession();
		User user = (User) session.load(User.class, id);
		closeSession();
		return user;
	}

	@Override
	public void updateProduct(Product product) {
		openSession();
		Product productNew = (Product) session.load(Product.class, product.getId());
		productNew.setName(product.getName());
		productNew.setKcal(product.getKcal());
		productNew.setProteins(product.getProteins());
		productNew.setFats(product.getFats());
		productNew.setCarbohydrates(product.getCarbohydrates());
		session.update(productNew);
		closeSession();
	}

	@Override
	public void addDayAndWeight(User user, Product product, Integer weight, String day) {
		openSession();
		Query query = session.createQuery("FROM UserProduct up WHERE up.id_user = :u_id AND up.id_product = :p_id  ");
		query.setParameter("u_id", user.getId());
		query.setParameter("p_id", product.getId());
		UserProduct userProduct = (UserProduct) query.list().get(query.list().size() - 1);
		WeightDay weightDay = new WeightDay(weight, day);
		userProduct.setWeightDay(weightDay);
		weightDay.setUserProduct(userProduct);
		session.save(userProduct);
		closeSession();
	}

	@Override
	public List<UserProduct> getUserProduct(User user, Product product) {
		openSession();
		Query query = session.createQuery("FROM UserProduct up WHERE up.id_user = :u_id AND up.id_product = :p_id  ");
		query.setParameter("u_id", user.getId());
		query.setParameter("p_id", product.getId());
		List<UserProduct> userProduct = query.list();
		closeSession();
		return userProduct;
	}

	@Override
	public boolean checkUserProductDay(User user, Product product, String day) {
		openSession();
		Query query = session.createQuery("FROM UserProduct up WHERE up.id_user = :u_id AND up.id_product = :p_id");
		query.setParameter("u_id", user.getId());
		query.setParameter("p_id", product.getId());
		List<UserProduct> userProducts = query.list();
		for (UserProduct userProduct : userProducts) {
			if (userProduct.getWeightDay().getDay().equals(day)) {
				return false; // this product has already existed in this day
			}
		}
		closeSession();
		return true;
	}

	@Override
	public void saveUserProduct(Product product, User user) {
		openSession();
		UserProduct userProduct = new UserProduct(user.getId(), product.getId());
		session.save(userProduct);
		closeSession();
	}

	@Override
	public void deleteUserProductById(int idUserProduct) {
		openSession();
		UserProduct userProduct = new UserProduct();
		userProduct.setId_userPoducts(idUserProduct);
		session.delete(userProduct);
		closeSession();
	}

	@Override
	public void deleteWeightDayById(int idUserProduct) {
		openSession();
		WeightDay weightDay = new WeightDay();
		weightDay.setId_userPoducts(idUserProduct);
		session.delete(weightDay);
		closeSession();
	}
}
