package DAO;

import java.util.List;

import model.Product;
import model.User;
import model.UserProduct;

public interface DAOfitSystem {

	public void addUserInDB(User user);

	public boolean checkUniquenessOfLogin(String login);

	public boolean checkUniquenessOfEmail(String eMail);

	public boolean checkLoginAndPassword(String login, String password);

	public Product getProduct(int idProduct);

	public List<User> getAllUsers();

	public User getUserByLogin(String login);

	public void deleteUser(int id);

	public void saveChangeUser(User user);

	public void addProduct(Product product);

	public boolean checkUniquenessOfNameProduct(String name);

	public List<Product> getAllProducts();

	public void deleteProduct(int id);

	public User getUserById(int id);

	public void addDayAndWeight(User user, Product product, Integer weight, String day);

	public void updateProduct(Product product);

	public List<UserProduct> getUserProduct(User user, Product product);

	public boolean checkUserProductDay(User user, Product product, String day);

	public void saveUserProduct(Product product, User user);

	public void deleteUserProductById(int idUserProduct);

	public void deleteWeightDayById(int idUserProduct);
}
