package model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "users")
public class User {
	@Id
	@Column(name = "id")
	@GeneratedValue
	private int id;

	@Column(name = "firstName")
	private String firstName;

	@Column(name = "lastName")
	private String lastName;

	@Column(name = "age")
	private int age;

	@Column(name = "eMail")
	private String eMail;

	@Column(name = "phone")
	private String phone;

	@Column(name = "login")
	private String login;

	@Column(name = "password")
	private String password;

	@Column(name = "role")
	private String role;

	@Column(name = "enabled")
	private boolean enabled;

	@ManyToMany(cascade = (CascadeType.ALL))
	@JoinTable(name = "userproducts", joinColumns = { @JoinColumn(name = "id_user") }, inverseJoinColumns = { @JoinColumn(name = "id_product") })
	private Set<Product> setProduct = new HashSet<Product>();

	public User() {
		super();
	}

	public User(String firstName, String lastName, int age, String eMail, String phone, String login, String password, String role, HashSet<Product> setProduct) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.age = age;
		this.eMail = eMail;
		this.phone = phone;
		this.login = login;
		this.password = password;
		this.role = role;
		this.setProduct = setProduct;
	}

	public User(int id, String firstName, String lastName, int age, String eMail, String phone, String login, String password, String role, Set<Product> setProduct) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.age = age;
		this.eMail = eMail;
		this.phone = phone;
		this.login = login;
		this.password = password;
		this.role = role;
		this.setProduct = setProduct;
	}

	public User(String firstName, String lastName, int age, String eMail, String phone, String login, String password, String role, boolean enabled, Set<Product> setProduct) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.age = age;
		this.eMail = eMail;
		this.phone = phone;
		this.login = login;
		this.password = password;
		this.role = role;
		this.enabled = enabled;
		this.setProduct = setProduct;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String geteMail() {
		return eMail;
	}

	public void seteMail(String eMail) {
		this.eMail = eMail;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public Set<Product> getSetProduct() {
		return setProduct;
	}

	public void setSetProduct(Set<Product> setProduct) {
		this.setProduct = setProduct;
	}

	@Override
	public String toString() {
		String result = "";
		result += id + " " + firstName + " " + lastName + " " + age + " " + eMail + " " + phone + " " + login + " " + password + "\n";
		return result;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

}
