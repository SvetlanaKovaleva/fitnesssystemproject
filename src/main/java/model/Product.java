package model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "products")
public class Product {
	@Id
	@Column(name = "id")
	@GeneratedValue
	private int id;

	@Column(name = "name")
	private String name;

	@Column(name = "kcal")
	private int kcal;

	@Column(name = "proteins")
	private int proteins;

	@Column(name = "fats")
	private int fats;

	@Column(name = "carbohydrates")
	private int carbohydrates;

	@ManyToMany(cascade = (CascadeType.ALL))
	@JoinTable(name = "userproducts", joinColumns = { @JoinColumn(name = "id_product") }, inverseJoinColumns = { @JoinColumn(name = "id_user") })
	private Set<User> setUser = new HashSet<User>();

	public Product(int id, String name, int kcal, int proteins, int fats, int carbohydrates, Set<User> setUser) {
		super();
		this.id = id;
		this.name = name;
		this.kcal = kcal;
		this.proteins = proteins;
		this.fats = fats;
		this.carbohydrates = carbohydrates;
		this.setUser = setUser;
	}

	public Product() {
		super();
	}

	public Product(String name, int kcal, int proteins, int fats, int carbohydrates, HashSet<User> setUser) {
		super();
		this.name = name;
		this.kcal = kcal;
		this.proteins = proteins;
		this.fats = fats;
		this.carbohydrates = carbohydrates;
		this.setUser = setUser;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getKcal() {
		return kcal;
	}

	public void setKcal(int kcal) {
		this.kcal = kcal;
	}

	public int getProteins() {
		return proteins;
	}

	public void setProteins(int proteins) {
		this.proteins = proteins;
	}

	public int getFats() {
		return fats;
	}

	public void setFats(int fats) {
		this.fats = fats;
	}

	public int getCarbohydrates() {
		return carbohydrates;
	}

	public void setCarbohydrates(int carbohydrates) {
		this.carbohydrates = carbohydrates;
	}

	public Set<User> getSetUser() {
		return setUser;
	}

	public void setSetUser(Set<User> setUser) {
		this.setUser = setUser;
	}

	@Override
	public String toString() {
		String result = "";
		result += id + " " + name + " " + kcal + " " + proteins + " " + fats + " " + carbohydrates + "\n";
		return result;
	}

}
