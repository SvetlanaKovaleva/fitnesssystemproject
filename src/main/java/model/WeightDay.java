package model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

@Entity
@Table(name = "weightday")
public class WeightDay {
	@Id
	@GeneratedValue(generator = "gen")
	// generator of keys in subordinate class
	@GenericGenerator(name = "gen", strategy = "foreign", parameters = @Parameter(name = "property", value = "userProduct"))
	// name instance of a class "@OneToOne @PrimaryKeyJoinColumn private UserProduct userProduct";
	@Column(name = "id_userPoducts")
	private int id_userPoducts;

	@Column(name = "weight")
	private int weight;

	@Column(name = "day")
	private String day;

	@OneToOne
	@PrimaryKeyJoinColumn
	private UserProduct userProduct;

	public WeightDay() {
		super();
	}

	public WeightDay(int weight, String day) {
		super();
		this.weight = weight;
		this.day = day;
	}

	public WeightDay(int weight, String day, UserProduct userProduct) {
		super();
		this.weight = weight;
		this.day = day;
		this.userProduct = userProduct;
	}

	public int getId_userPoducts() {
		return id_userPoducts;
	}

	public void setId_userPoducts(int id_userPoducts) {
		this.id_userPoducts = id_userPoducts;
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

	public String getDay() {
		return day;
	}

	public void setDay(String day) {
		this.day = day;
	}

	public UserProduct getUserProduct() {
		return userProduct;
	}

	public void setUserProduct(UserProduct userProduct) {
		this.userProduct = userProduct;
	}
}
