package model;

public class DayTotal {
	private String day;
	private int kCalTotal = 0;
	private int proteinsTotal = 0;
	private int fatsTotal = 0;
	private int carbohydratesTotal = 0;

	public DayTotal(String day) {
		super();
		this.day = day;
	}

	public DayTotal(int kCalTotal, int proteinsTotal, int fatsTotal, int carbohydratesTotal) {
		super();
		this.kCalTotal = kCalTotal;
		this.proteinsTotal = proteinsTotal;
		this.fatsTotal = fatsTotal;
		this.carbohydratesTotal = carbohydratesTotal;
	}

	public String getDay() {
		return day;
	}

	public void setDay(String day) {
		this.day = day;
	}

	public int getkCalTotal() {
		return kCalTotal;
	}

	public void setkCalTotal(int kCalTotal) {
		this.kCalTotal = kCalTotal;
	}

	public int getProteinsTotal() {
		return proteinsTotal;
	}

	public void setProteinsTotal(int proteinsTotal) {
		this.proteinsTotal = proteinsTotal;
	}

	public int getFatsTotal() {
		return fatsTotal;
	}

	public void setFatsTotal(int fatsTotal) {
		this.fatsTotal = fatsTotal;
	}

	public int getCarbohydratesTotal() {
		return carbohydratesTotal;
	}

	public void setCarbohydratesTotal(int carbohydratesTotal) {
		this.carbohydratesTotal = carbohydratesTotal;
	}
}
