package model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "userproducts")
public class UserProduct {
	@Id
	@GeneratedValue
	@Column(name = "id_userPoducts")
	private int id_userPoducts;

	@Column(name = "id_user")
	private int id_user;

	@Column(name = "id_product")
	private int id_product;

	@OneToOne(mappedBy = "userProduct", cascade = CascadeType.ALL)
	// mappadBy ="userProduct" this is name of object "private UserProduct userProduct; " in the class WeightDay
	private WeightDay weightDay;

	public UserProduct() {
		super();
	}

	public UserProduct(int id_user, int id_product) {
		super();
		this.id_user = id_user;
		this.id_product = id_product;
	}

	public UserProduct(int id_userPoducts, int id_user, int id_product, WeightDay weightDay) {
		super();
		this.id_userPoducts = id_userPoducts;
		this.id_user = id_user;
		this.id_product = id_product;
		this.weightDay = weightDay;
	}

	public int getId_userPoducts() {
		return id_userPoducts;
	}

	public void setId_userPoducts(int id_userPoducts) {
		this.id_userPoducts = id_userPoducts;
	}

	public int getId_user() {
		return id_user;
	}

	public void setId_user(int id_user) {
		this.id_user = id_user;
	}

	public int getId_product() {
		return id_product;
	}

	public void setId_product(int id_product) {
		this.id_product = id_product;
	}

	public WeightDay getWeightDay() {
		return weightDay;
	}

	public void setWeightDay(WeightDay weightDay) {
		this.weightDay = weightDay;
	}

}
