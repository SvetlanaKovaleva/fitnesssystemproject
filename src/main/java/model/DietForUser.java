package model;

public class DietForUser {
	private int id_userPoducts;
	private String dayOfAWeek;
	private String nameProduct;
	private int weight;
	private int kcal;
	private int proteins;
	private int fats;
	private int carbohydrates;

	public DietForUser() {
		super();
	}

	public DietForUser(String dayOfAWeek, String nameProduct, int weight, int kcal, int proteins, int fats, int carbohydrates) {
		super();
		this.dayOfAWeek = dayOfAWeek;
		this.nameProduct = nameProduct;
		this.weight = weight;
		this.kcal = kcal;
		this.proteins = proteins;
		this.fats = fats;
		this.carbohydrates = carbohydrates;
	}

	public int getId_userPoducts() {
		return id_userPoducts;
	}

	public void setId_userPoducts(int id_userPoducts) {
		this.id_userPoducts = id_userPoducts;
	}

	public String getDayOfAWeek() {
		return dayOfAWeek;
	}

	public void setDayOfAWeek(String dayOfAWeek) {
		this.dayOfAWeek = dayOfAWeek;
	}

	public String getNameProduct() {
		return nameProduct;
	}

	public void setNameProduct(String nameProduct) {
		this.nameProduct = nameProduct;
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

	public int getKcal() {
		return kcal;
	}

	public void setKcal(int kcal) {
		this.kcal = kcal;
	}

	public int getProteins() {
		return proteins;
	}

	public void setProteins(int proteins) {
		this.proteins = proteins;
	}

	public int getFats() {
		return fats;
	}

	public void setFats(int fats) {
		this.fats = fats;
	}

	public int getCarbohydrates() {
		return carbohydrates;
	}

	public void setCarbohydrates(int carbohydrates) {
		this.carbohydrates = carbohydrates;
	}
}
