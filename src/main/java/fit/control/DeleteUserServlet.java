package fit.control;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import model.User;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import DAO.DAOfitSystemImpl;

@Controller
public class DeleteUserServlet {
	@RequestMapping(value = "/deleteUserFitnessSystem", method = RequestMethod.POST)
	public ModelAndView deleteUserFitnessSystem(@RequestParam Map<String, String> params) throws IOException {
		ModelAndView mv;
		DAOfitSystemImpl daoFitSystemImpl = new DAOfitSystemImpl();
		List<User> listAllUsers = daoFitSystemImpl.getAllUsers();
		for (int i = listAllUsers.size() - 1; i >= 0; i--) {
			String currentIdUser = params.get(Integer.toString(listAllUsers.get(i).getId()));
			if (currentIdUser != null) {
				daoFitSystemImpl.deleteUser(listAllUsers.get(i).getId());
				listAllUsers.remove(i);
			}
		}
		mv = new ModelAndView("removeUser");
		mv.addObject("listAllUsers", listAllUsers);
		return mv;
	}
}
