package fit.control;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import model.Product;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import DAO.DAOfitSystemImpl;

@Controller
public class AddProductServlet {
	@RequestMapping(value = "/addProductFitnessSystem", method = RequestMethod.POST)
	public ModelAndView addProductFitnessSystem(@RequestParam Map<String, String> params) throws IOException {
		DAOfitSystemImpl daoFitSystemImpl = new DAOfitSystemImpl();
		List<Product> listAllProducts = daoFitSystemImpl.getAllProducts();
		ModelAndView mv = new ModelAndView("addProduct");
		mv.addObject("listAllProducts", listAllProducts);
		return mv;
	}

}
