package fit.control;

import java.io.IOException;
import java.security.Principal;
import java.util.Map;

import model.User;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

@Controller
@SessionAttributes(value = { "DietUser" })
public class BackToWelcome {
	@RequestMapping(value = "/backToWelcomeFitnessSystem", method = RequestMethod.POST)
	public ModelAndView backToWelcomeFitnessSystem(@RequestParam Map<String, String> params, @ModelAttribute("DietUser") User dietUser, Principal principal) throws IOException {
		ModelAndView mv;
		dietUser = null;
		mv = new ModelAndView("welcome");
		mv.addObject("UserName", principal.getName());
		return mv;
	}

}
