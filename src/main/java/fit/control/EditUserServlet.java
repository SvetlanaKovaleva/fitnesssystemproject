package fit.control;

import java.io.IOException;
import java.security.Principal;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import model.User;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import DAO.DAOfitSystemImpl;

@Controller
public class EditUserServlet {
	@RequestMapping(value = "/editUserFitnessSystem", method = RequestMethod.POST)
	public ModelAndView editUserFitnessSystem(@RequestParam Map<String, String> params, Principal principal) throws IOException {

		String idNew = params.get("id");
		String firstName = params.get("firstName");
		String lastName = params.get("lastName");
		String age = params.get("age");
		String eMail = params.get("eMail");
		String phone = params.get("phone");
		String login = params.get("login");
		String password = params.get("password");
		String role = params.get("role");
		ModelAndView mv;
		DAOfitSystemImpl daoFitSystemImpl = new DAOfitSystemImpl();
	
		if (!idNew.equals("")) {
			int id = Integer.parseInt(idNew);
			Pattern p = Pattern.compile(".+\\.(com|ua|ru)");
			Matcher m = p.matcher(eMail);
			Pattern pAge = Pattern.compile("^[0-9]+");
			Matcher mAge = pAge.matcher(age);
			User user = daoFitSystemImpl.getUserById(id);
			if (!login.equals("") && !password.equals("") && !firstName.equals("") && !lastName.equals("") && !eMail.equals("") && !phone.equals("") && !age.equals("") && !role.equals("")) {
				if (daoFitSystemImpl.checkUniquenessOfLogin(login) == true || login.equals(user.getLogin())) {
					if (eMail.indexOf("@") != -1 && m.matches() == true) {
						if (mAge.matches() == true && age.length() > 1) {
							user.setFirstName(firstName);
							user.setLastName(lastName);
							user.setAge(Integer.parseInt(age));
							user.seteMail(eMail);
							user.setPhone(phone);
							user.setLogin(login);
							user.setPassword(password);
							user.setRole(role);
							daoFitSystemImpl.saveChangeUser(user);
							mv = new ModelAndView("showUsers");
							List<User> listAllUsers = daoFitSystemImpl.getAllUsers();
							mv.addObject("listAllUsers", listAllUsers);
							mv.addObject("UserName", principal.getName());
						} else {
							List<User> listAllUsers = daoFitSystemImpl.getAllUsers();
							mv = new ModelAndView("showUsers");
							mv.addObject("listAllUsers", listAllUsers);
							mv.addObject("errorAge", "Please enter correct age");
						}
					} else {
						List<User> listAllUsers = daoFitSystemImpl.getAllUsers();
						mv = new ModelAndView("showUsers");
						mv.addObject("listAllUsers", listAllUsers);
						mv.addObject("errorEMail", "Please enter correct e-mail");
					}
				} else {
					List<User> listAllUsers = daoFitSystemImpl.getAllUsers();
					mv = new ModelAndView("showUsers");
					mv.addObject("listAllUsers", listAllUsers);
					mv.addObject("errorLoginExist", "This login has already exist");
				}
			} else {
				List<User> listAllUsers = daoFitSystemImpl.getAllUsers();
				mv = new ModelAndView("showUsers");
				mv.addObject("listAllUsers", listAllUsers);
				mv.addObject("errorEmpty", "Exist empty fields");
			}
		} else {
			List<User> listAllUsers = daoFitSystemImpl.getAllUsers();
			mv = new ModelAndView("showUsers");
			mv.addObject("listAllUsers", listAllUsers);
			mv.addObject("errorEmpty", "Exist empty fields");
		}
		return mv;
	}
}
