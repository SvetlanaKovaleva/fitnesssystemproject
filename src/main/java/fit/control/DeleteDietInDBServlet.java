package fit.control;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import model.Product;
import model.User;
import model.UserProduct;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import DAO.DAOfitSystemImpl;

@Controller
@SessionAttributes(value = ("DietUser"))
public class DeleteDietInDBServlet {
	@RequestMapping(value = "/deleteDietInDBFitnessSystem", method = RequestMethod.POST)
	public ModelAndView deleteDietInDBFitnessSystem(@RequestParam Map<String, String> params, @ModelAttribute("DietUser")User dietUser) throws IOException {
		ModelAndView mv;
		DAOfitSystemImpl daoFitSystemImpl = new DAOfitSystemImpl();
		User user = dietUser;
		List<UserProduct> userProducts = new ArrayList<UserProduct>();
		for (Product product : user.getSetProduct()) {
			userProducts = daoFitSystemImpl.getUserProduct(user, product);
			for (int i = userProducts.size() - 1; i >= 0; i--) {
				String currentId = params.get(Integer.toString(userProducts.get(i).getId_userPoducts()));
				if (currentId != null) {
					int id = userProducts.get(i).getId_userPoducts();
					daoFitSystemImpl.deleteUserProductById(id);
					daoFitSystemImpl.deleteWeightDayById(id);
				}
			}
		}
		mv = new ModelAndView("deleteDietOk");
		mv.addObject("userProducts", userProducts);
		return mv;
	}
}
