package fit.control;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import model.User;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import DAO.DAOfitSystemImpl;

@Controller
public class SelectUserServlet {
	@RequestMapping(value = "/selectUserFitnessSystem", method = RequestMethod.POST)
	public ModelAndView selectUserFitnessSystem(@RequestParam Map<String, String> params) throws IOException {
		DAOfitSystemImpl daoFitSystemImpl = new DAOfitSystemImpl();
		User user = daoFitSystemImpl.getUserByLogin("login");
		List<User> listUser = new ArrayList<User>();
		listUser.add(user);
		ModelAndView mv = new ModelAndView("editUser");
		return mv;
	}
}
