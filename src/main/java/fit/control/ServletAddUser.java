package fit.control;

import java.io.IOException;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ServletAddUser {
	@RequestMapping(value = "/addUserFitnessSystem", method = RequestMethod.POST)
	public ModelAndView addUserFitnessSystem(@RequestParam Map<String, String> params) throws IOException {
		ModelAndView mv = new ModelAndView("registration");
		return mv;
	}
}
