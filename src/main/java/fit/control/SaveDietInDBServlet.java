package fit.control;

import java.io.IOException;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import model.Product;
import model.User;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import DAO.DAOfitSystemImpl;

@Controller
@SessionAttributes(value = ("DietUser"))
public class SaveDietInDBServlet {
	@RequestMapping(value = "/saveDietInDBFitnessSystem", method = RequestMethod.POST)
	public ModelAndView saveDietInDBFitnessSystem(@RequestParam Map<String, String> params, @ModelAttribute("DietUser")User dietUser) throws IOException {
		ModelAndView mv;
		DAOfitSystemImpl daoFitSystemImpl = new DAOfitSystemImpl();
		String id_product = params.get("idProducts");
		String weight = params.get("weight");
		String day = params.get("day");
		User user = dietUser;
		Pattern p = Pattern.compile("^[0-9]+");
		Matcher m = p.matcher(weight);

		if (id_product != null && weight != null && day != null) {
			if (m.matches() == true) {
				Product product = daoFitSystemImpl.getProduct(Integer.parseInt(id_product));
				if (daoFitSystemImpl.checkUserProductDay(user, product, day)) {
					daoFitSystemImpl.saveUserProduct(product, user);
					daoFitSystemImpl.addDayAndWeight(user, product, Integer.parseInt(weight), day);
					mv = new ModelAndView("createTableOk");
				} else {
					mv = new ModelAndView("createTableOk");
					mv.addObject("errorCheckProduct", "This product has already existed in this day, try again");
				}
			} else {
				mv = new ModelAndView("createTableOk");
				mv.addObject("errorWeight", "Please enter correct weight");
			}
		} else {
			mv = new ModelAndView("createTableOk");
			mv.addObject("errorEmpty", "Exist empty fields");
		}
		return mv;
	}
}
