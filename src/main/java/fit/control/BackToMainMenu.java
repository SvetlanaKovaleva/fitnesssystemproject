package fit.control;

import java.io.IOException;
import java.security.Principal;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class BackToMainMenu {
	@RequestMapping("/backToMainMenuFitnessSystem")
	public ModelAndView backToMainMenuFitnessSystem(@RequestParam Map<String, String> params, Principal principal) throws IOException {
		ModelAndView mv = new ModelAndView("welcome");
		mv.addObject("UserName", principal.getName());
		return mv;
	}
}
