package fit.control;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class LoginServlet {
	@RequestMapping("/loginFitnessSystem")
	public ModelAndView login(@RequestParam(value = "error", required = false) String error, @RequestParam(value = "logout", required = false) String logout) {
		if (error != null) {
			error = "Please enter correct user NAME and PASSWORD";
		}
		if (logout != null) {
			logout = "Logout complete !";
		}
		ModelAndView mv = new ModelAndView("login");
		mv.addObject("error", error);
		mv.addObject("logout", logout);
		return mv;

	}

}
