package fit.control;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import model.Product;
import model.User;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import DAO.DAOfitSystemImpl;

@Controller
public class SaveProductInDB {
	@RequestMapping(value = "/saveProductInDBFitnessSystem", method = RequestMethod.POST)
	public ModelAndView saveProductInDBFitnessSystem(@RequestParam Map<String, String> params) throws IOException {
		String name = params.get("name");
		String kcal = params.get("kcal");
		String proteins = params.get("proteins");
		String fats = params.get("fats");
		String carbohydrates = params.get("carbohydrates");
		DAOfitSystemImpl daoFitSystemImpl = new DAOfitSystemImpl();
		List<Product> listAllProducts;
		ModelAndView mv;
		Pattern p = Pattern.compile("^[0-9]+");
		Matcher mKcal = p.matcher(kcal);
		Matcher mProteins = p.matcher(proteins);
		Matcher mFats = p.matcher(fats);
		Matcher mCarbohydrates = p.matcher(carbohydrates);

		if (!name.equals("") && !kcal.equals("") && !proteins.equals("") && !fats.equals("") && !carbohydrates.equals("")) {
			if (mKcal.matches() == true && mProteins.matches() == true && mFats.matches() == true && mCarbohydrates.matches() == true) {
				if (daoFitSystemImpl.checkUniquenessOfNameProduct(name) == true) {
					daoFitSystemImpl.addProduct(new Product(name, Integer.parseInt(kcal), Integer.parseInt(proteins), Integer.parseInt(fats), Integer.parseInt(carbohydrates), new HashSet<User>()));
					listAllProducts = daoFitSystemImpl.getAllProducts();
					mv = new ModelAndView("addProduct");
					mv.addObject("listAllProducts", listAllProducts);
				} else {
					listAllProducts = daoFitSystemImpl.getAllProducts();
					mv = new ModelAndView("addProduct");
					mv.addObject("listAllProducts", listAllProducts);
					mv.addObject("errorNameProduct", "This product has already existed.");
				}
			} else {
				listAllProducts = daoFitSystemImpl.getAllProducts();
				mv = new ModelAndView("addProduct");
				mv.addObject("listAllProducts", listAllProducts);
				mv.addObject("errorNotCorrectFilds", "Please, check for correct this field (only integer).");
			}
		} else {
			listAllProducts = daoFitSystemImpl.getAllProducts();
			mv = new ModelAndView("addProduct");
			mv.addObject("listAllProducts", listAllProducts);
			mv.addObject("errorEmptyFilds", "Exist empty fields");
		}
		return mv;
	}
}
