package fit.control;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

import model.DayTotal;
import model.DietForUser;
import model.Product;
import model.User;
import model.UserProduct;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import DAO.DAOfitSystemImpl;

@Controller
public class Welcome {
	@RequestMapping("/welcome")
	public ModelAndView goToPageAdminWelcome(Principal principal) {
		ModelAndView mv;
		String login = principal.getName();
		DAOfitSystemImpl daoFitSystemImpl = new DAOfitSystemImpl();
		DayTotal[] dayTotal = { new DayTotal("Monday"), new DayTotal("Tuesday"), new DayTotal("Wednesday"), new DayTotal("Thursday"), new DayTotal("Friday"), new DayTotal("Saturday"), new DayTotal("Sunday") };
		User user = daoFitSystemImpl.getUserByLogin(login);
		List<DietForUser> dfu = new ArrayList<DietForUser>();
		for (Product product : user.getSetProduct()) {
			List<UserProduct> userProducts = daoFitSystemImpl.getUserProduct(user, product);
			for (UserProduct up : userProducts) {
				DietForUser dietForUser = new DietForUser();
				dietForUser.setId_userPoducts(up.getId_userPoducts());
				dietForUser.setDayOfAWeek(up.getWeightDay().getDay());
				dietForUser.setNameProduct(product.getName());
				dietForUser.setWeight(up.getWeightDay().getWeight());
				dietForUser.setKcal(product.getKcal() * up.getWeightDay().getWeight() / 100);
				dietForUser.setProteins(product.getProteins() * up.getWeightDay().getWeight() / 100);
				dietForUser.setFats(product.getFats() * up.getWeightDay().getWeight() / 100);
				dietForUser.setCarbohydrates(product.getCarbohydrates() * up.getWeightDay().getWeight() / 100);
				dfu.add(dietForUser);
			}
		}
		for (DayTotal dt : dayTotal) {
			for (DietForUser resultUserProducts : dfu) {
				if (dt.getDay().equals(resultUserProducts.getDayOfAWeek())) {
					dt.setkCalTotal(dt.getkCalTotal() + resultUserProducts.getKcal());
					dt.setProteinsTotal(dt.getProteinsTotal() + resultUserProducts.getProteins());
					dt.setFatsTotal(dt.getFatsTotal() + resultUserProducts.getFats());
					dt.setCarbohydratesTotal(dt.getCarbohydratesTotal() + resultUserProducts.getCarbohydrates());
				}
			}
		}
		mv = new ModelAndView("welcome");
		mv.addObject("listResultUserProducts", dfu); 
		mv.addObject("dayTotal", dayTotal); 
		User User = daoFitSystemImpl.getUserByLogin(login);
		mv.addObject("UserName", principal.getName());
		mv.addObject("User", user);
		return mv;
	}
}
