package fit.control;

import java.io.IOException;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import model.DayTotal;
import model.DietForUser;
import model.Product;
import model.User;
import model.UserProduct;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import DAO.DAOfitSystemImpl;

@Controller
@SessionAttributes(value = { "DietUser" })
public class SelectUserForCreateTableDiet {
	@ModelAttribute("DietUser")
	// method will ignore if @SessionAttributes(value = ("DietUser")) exists.
	public User createUser() {
		return new User();
	}

	@RequestMapping(value = "/selectUserForCreateTableDiet", method = RequestMethod.POST)
	public ModelAndView selectUserForCreateTableDiet(@RequestParam Map<String, String> params, @ModelAttribute("DietUser") User DietUser, Principal principal) throws IOException {
		String idUser = params.get("loginUserList");
		ModelAndView mv;
		DAOfitSystemImpl daoFitSystemImpl = new DAOfitSystemImpl();
		List<Product> listAllProducts = daoFitSystemImpl.getAllProducts();
		DayTotal[] dayTotal = { new DayTotal("Monday"), new DayTotal("Tuesday"), new DayTotal("Wednesday"), new DayTotal("Thursday"), new DayTotal("Friday"), new DayTotal("Saturday"), new DayTotal("Sunday") };
			if (idUser != null) {
				DietUser = daoFitSystemImpl.getUserById(Integer.parseInt(idUser));
			}else {
				DietUser = daoFitSystemImpl.getUserById(DietUser.getId());
			}
			List<DietForUser> dfu = new ArrayList<DietForUser>();
			List<UserProduct> userProducts = new ArrayList<UserProduct>();
			List<UserProduct> userProductsNew = new ArrayList<UserProduct>();
			for (Product product : DietUser.getSetProduct()) {
				userProducts = daoFitSystemImpl.getUserProduct(DietUser, product);
				for (UserProduct up : userProducts) {
					userProductsNew.add(up);
					DietForUser dietForUser = new DietForUser();
					dietForUser.setId_userPoducts(up.getId_userPoducts());
					dietForUser.setDayOfAWeek(up.getWeightDay().getDay());
					dietForUser.setNameProduct(product.getName());
					dietForUser.setWeight(up.getWeightDay().getWeight());
					dietForUser.setKcal(product.getKcal() * up.getWeightDay().getWeight() / 100);
					dietForUser.setProteins(product.getProteins() * up.getWeightDay().getWeight() / 100);
					dietForUser.setFats(product.getFats() * up.getWeightDay().getWeight() / 100);
					dietForUser.setCarbohydrates(product.getCarbohydrates() * up.getWeightDay().getWeight() / 100);
					dfu.add(dietForUser);
				}
			}
			for (DayTotal dt : dayTotal) {
				for (DietForUser resultUserProducts : dfu) {
					if (dt.getDay().equals(resultUserProducts.getDayOfAWeek())) {
						dt.setkCalTotal(dt.getkCalTotal() + resultUserProducts.getKcal());
						dt.setProteinsTotal(dt.getProteinsTotal() + resultUserProducts.getProteins());
						dt.setFatsTotal(dt.getFatsTotal() + resultUserProducts.getFats());
						dt.setCarbohydratesTotal(dt.getCarbohydratesTotal() + resultUserProducts.getCarbohydrates());
					}
				}
			}
			mv = new ModelAndView("diet");
			mv.addObject("userProducts", userProductsNew);
			mv.addObject("listResultUserProducts", dfu);
			mv.addObject("listAllProducts", listAllProducts);
			mv.addObject("dayTotal", dayTotal);
			mv.addObject("login", DietUser.getLogin());
			mv.addObject("DietUser", DietUser);
		return mv;
	}

}
