package fit.control;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import model.Product;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import DAO.DAOfitSystemImpl;

@Controller
public class DeleteProduct {
	@RequestMapping(value = "/deleteProductFitnessSystem", method = RequestMethod.POST)
	public ModelAndView deleteProductFitnessSystem(@RequestParam Map<String, String> params) throws IOException {
		ModelAndView mv;
		DAOfitSystemImpl daoFitSystem = new DAOfitSystemImpl();
		List<Product> listAllProducts = daoFitSystem.getAllProducts();
		for (int i = listAllProducts.size() - 1; i >= 0; i--) {
			String currentProduct = params.get(Integer.toString(listAllProducts.get(i).getId()));
			if (currentProduct != null) {
				daoFitSystem.deleteProduct(listAllProducts.get(i).getId());
				listAllProducts.remove(i);
			}
		}
		mv = new ModelAndView("removeProduct");
		mv.addObject("listAllProducts", listAllProducts);
		return mv;
	}
}
