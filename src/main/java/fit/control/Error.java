package fit.control;

import java.io.IOException;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class Error {
	@RequestMapping("/error")
	public ModelAndView errorFitnessSystem(@RequestParam Map<String, String> params) throws IOException {
		ModelAndView mv = new ModelAndView("error");
		return mv;
	}
}
