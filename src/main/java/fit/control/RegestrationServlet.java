package fit.control;

import java.io.IOException;
import java.security.Principal;
import java.util.HashSet;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import model.Product;
import model.User;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import DAO.DAOfitSystemImpl;

@Controller
public class RegestrationServlet {
	@RequestMapping(value = "/regestrationFitnessSystem", method = RequestMethod.POST)
	public ModelAndView regestrationFitnessSystem(@RequestParam Map<String, String> params, Principal principal) throws IOException {
		String login = params.get("loginNew");
		String password = params.get("passwordNew");
		String password2 = params.get("password2");
		String age = params.get("ageNew");
		String firstName = params.get("firstName");
		String lastName = params.get("lastName");
		String eMail = params.get("eMail");
		String phone = params.get("phone");
		ModelAndView mv = null;
		DAOfitSystemImpl daoFitSystemImpl = new DAOfitSystemImpl();
		Pattern p = Pattern.compile(".+\\.(com|ua|ru)");
		Matcher m = p.matcher(eMail);
		Pattern pAge = Pattern.compile("^[0-9]+");
		Matcher mAge = pAge.matcher(age);

		if (!login.equals("") && !password.equals("") && !password2.equals("") && !firstName.equals("") && !lastName.equals("") && !eMail.equals("") && !phone.equals("") && !age.equals("")) {
			if (password.equals(password2)) {
				if (eMail.indexOf("@") != -1 && m.matches() == true) {
					if (daoFitSystemImpl.checkUniquenessOfEmail(eMail) == true) {
						if (mAge.matches() == true && age.length() > 1) {
							if (daoFitSystemImpl.checkUniquenessOfLogin(login) == true) {
								User user = new User(firstName, lastName, Integer.parseInt(age), eMail, phone, login, password2, "ROLE_USER", true, new HashSet<Product>());
								daoFitSystemImpl.addUserInDB(user);
								if (user.getRole().equals("ROLE_USER")) {
									mv = new ModelAndView("login");
								} else {
									mv = new ModelAndView("welcome");
									mv.addObject("UserName", principal.getName());
								}
							} else {
								mv = new ModelAndView("registration");
								mv.addObject("errorLogin", "This login has already existed.");
							}
						} else {
							mv = new ModelAndView("registration");
							mv.addObject("errorAge", "Please enter correct age.");
						}
					} else {
						mv = new ModelAndView("registration");
						mv.addObject("errorUniqEMail", "This e-mail has already existed.");
					}
				} else {
					mv = new ModelAndView("registration");
					mv.addObject("errorEMail", "Please enter correct e-mail");
				}
			} else {
				mv = new ModelAndView("registration");
				mv.addObject("errorPassword", "Passwords are not equals");
			}
		} else {
			mv = new ModelAndView("registration");
			mv.addObject("errorEmpty", "Exist empty fields");
		}
		return mv;
	}
}
