package fit.control;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import model.User;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import DAO.DAOfitSystemImpl;

@Controller
public class RemoveUserServlet {
	@RequestMapping(value = "/removeUserFitnessSystem", method = RequestMethod.POST)
	public ModelAndView removeUserFitnessSystem(@RequestParam Map<String, String> params) throws IOException {
		DAOfitSystemImpl daoFitSystemImpl = new DAOfitSystemImpl();
		List<User> listAllUsers = daoFitSystemImpl.getAllUsers();
		ModelAndView mv = new ModelAndView("removeUser");
		mv.addObject("listAllUsers", listAllUsers);
		return mv;
	}
}
