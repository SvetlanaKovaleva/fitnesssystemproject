package DAO;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import model.Product;
import model.User;
import model.UserProduct;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TestDAOfitSystemImpl {
	List<User> users = new ArrayList<User>();
	List<Product> products = new ArrayList<Product>();
	DAOfitSystemImpl daoFitSystemImpl;

	@Before
	public void setUp() throws Exception {
		daoFitSystemImpl = new DAOfitSystemImpl();
	}

	@After
	public void tearDown() throws Exception {
		System.out.println("Test has done");
	}

	@Test
	public void testAddUserInDB() {
		User user = new User("Tom", "Ford", 54, "t.ford@gmail.com", "380979065712", "TomFord", "123456", "User", new HashSet<Product>());
		daoFitSystemImpl.addUserInDB(user);
		User testUser = daoFitSystemImpl.getUserByLogin(user.getLogin());
		assertTrue(testUser.getFirstName().equals(user.getFirstName()));
		daoFitSystemImpl.deleteUser(testUser.getId());
	}

	@Test
	public void testCheckUniquenessOfLogin() {
		String existLogin = "User1";
		String failLogin = "Petr";
		assertTrue(!daoFitSystemImpl.checkUniquenessOfLogin(existLogin));
		assertTrue(daoFitSystemImpl.checkUniquenessOfLogin(failLogin));
	}

	@Test
	public void testCheckUniquenessOfEmail() {
		String existEmail = "user1@gmail.com";
		String failEmail = "petr@gmail.com";
		assertTrue(!daoFitSystemImpl.checkUniquenessOfEmail(existEmail));
		assertTrue(daoFitSystemImpl.checkUniquenessOfEmail(failEmail));
	}

	@Test
	public void testCheckLoginAndPassword() {
		String existLogin = "User1";
		String existPassword = "111111";
		String failLogin = "Petr";
		String failPassword = "pppppp";
		assertTrue(daoFitSystemImpl.checkLoginAndPassword(existLogin, existPassword));
		assertTrue(!daoFitSystemImpl.checkLoginAndPassword(failLogin, failPassword));
	}

	@Test
	public void testGetProduct() {
		List<Product> products = daoFitSystemImpl.getAllProducts();
		for (Product product : products) {
			assertTrue(product.getName().equals(daoFitSystemImpl.getProduct(product.getId()).getName()));
		}
	}

	@Test
	public void testGetAllUsers() {
		List<User> users = daoFitSystemImpl.getAllUsers();
		for (User user : users) {
			assertTrue(user.getLogin().equals(daoFitSystemImpl.getUserById(user.getId()).getLogin()));
		}
	}

	@Test
	public void testGetUserByLogin() {
		User user = daoFitSystemImpl.getUserByLogin("User1");
		User testUser = daoFitSystemImpl.getUserById(user.getId());
		assertTrue(user.getLastName().equals(testUser.getLastName()));
	}

	@Test
	public void testDeleteUser() {
		User user = new User("Tom", "Ford", 54, "t.ford@gmail.com", "380979065712", "TomFord", "123456", "User", new HashSet<Product>());
		daoFitSystemImpl.addUserInDB(user);
		List<User> users = daoFitSystemImpl.getAllUsers();
		int usersSize = users.size();
		daoFitSystemImpl.deleteUser(user.getId());
		List<User> testUsers = daoFitSystemImpl.getAllUsers();
		int testUsersSize = testUsers.size();
		assertTrue(usersSize == testUsersSize + 1);
	}

	@Test
	public void testSaveChangeUser() {
		User user = daoFitSystemImpl.getUserById(2);
		user.setAge(20);
		daoFitSystemImpl.saveChangeUser(user);
		assertTrue(daoFitSystemImpl.getUserByLogin(user.getLogin()).getAge() == 20);
		user.setAge(25);
		daoFitSystemImpl.saveChangeUser(user);
		assertTrue(daoFitSystemImpl.getUserByLogin(user.getLogin()).getAge() == 25);
	}

	@Test
	public void testAddProduct() {
		Product product = new Product("tomato", 20, 1, 0, 4, new HashSet<User>());
		daoFitSystemImpl.addProduct(product);
		Product testProduct = daoFitSystemImpl.getProduct(product.getId());
		assertTrue(testProduct.getName().equals(product.getName()));
		daoFitSystemImpl.deleteProduct(testProduct.getId());
	}

	@Test
	public void testCheckUniquenessOfNameProduct() {
		String existNameProduct = "apples";
		String failNameProduct = "coconut";
		assertTrue(!daoFitSystemImpl.checkUniquenessOfNameProduct(existNameProduct));
		assertTrue(daoFitSystemImpl.checkUniquenessOfNameProduct(failNameProduct));
	}

	@Test
	public void testGetAllProducts() {
		List<Product> products = daoFitSystemImpl.getAllProducts();
		for (Product product : products) {
			assertTrue(product.getName().equals(daoFitSystemImpl.getProduct(product.getId()).getName()));
		}
	}

	@Test
	public void testDeleteProduct() {
		Product product = new Product("tomato", 20, 1, 0, 4, new HashSet<User>());
		daoFitSystemImpl.addProduct(product);
		List<Product> products = daoFitSystemImpl.getAllProducts();
		int productsSize = products.size();
		daoFitSystemImpl.deleteProduct(product.getId());
		List<Product> productsTest = daoFitSystemImpl.getAllProducts();
		int productsTestSize = productsTest.size();
		assertTrue(productsSize == productsTestSize + 1);
	}

	@Test
	public void testGetUserById() {
		User user = daoFitSystemImpl.getUserById(2);
		User testUser = daoFitSystemImpl.getUserByLogin(user.getLogin());
		assertTrue(user.getLastName().equals(testUser.getLastName()));
	}

	@Test
	public void testUpdateProduct() {
		Product product = daoFitSystemImpl.getProduct(1);
		product.setCarbohydrates(14);
		daoFitSystemImpl.updateProduct(product);
		assertTrue(daoFitSystemImpl.getProduct(product.getId()).getCarbohydrates() == 14);
		product.setCarbohydrates(13);
		daoFitSystemImpl.updateProduct(product);
		assertTrue(daoFitSystemImpl.getProduct(product.getId()).getCarbohydrates() == 13);
	}

	@Test
	public void testAddDayAndWeight() {
		User user = daoFitSystemImpl.getUserByLogin("User3");
		Product product = daoFitSystemImpl.getProduct(1);
		String day = "Monday";
		int weight = 233;
		daoFitSystemImpl.saveUserProduct(product, user);
		daoFitSystemImpl.addDayAndWeight(user, product, weight, day);
		List<UserProduct> userProducts = daoFitSystemImpl.getUserProduct(user, product);
		UserProduct userProduct = userProducts.get(userProducts.size() - 1);
		assertTrue(userProduct.getWeightDay().getDay().equals(day));
		assertTrue(userProduct.getWeightDay().getWeight() == weight);
		int id = userProduct.getId_userPoducts();
		daoFitSystemImpl.deleteUserProductById(id);
		daoFitSystemImpl.deleteWeightDayById(id);
	}

	@Test
	public void testGetUserProduct() {
		User user = daoFitSystemImpl.getUserById(2);
		Product product = daoFitSystemImpl.getProduct(1);
		List<UserProduct> userProducts = daoFitSystemImpl.getUserProduct(user, product);
		for (UserProduct userProduct : userProducts) {
			assertTrue(userProduct.getId_product() == product.getId());
			assertTrue(userProduct.getId_user() == user.getId());
		}
	}

	@Test
	public void testCheckUserProductDay() {
		User user = daoFitSystemImpl.getUserById(2);
		Product product = daoFitSystemImpl.getProduct(1);
		String existDay = "Monday";
		String failDay = "Friday";
		assertTrue(!daoFitSystemImpl.checkUserProductDay(user, product, existDay));
		assertTrue(daoFitSystemImpl.checkUserProductDay(user, product, failDay));
	}

	@Test
	public void testSaveUserProduct() {
		User user = daoFitSystemImpl.getUserById(3);
		Product product = daoFitSystemImpl.getProduct(15);
		daoFitSystemImpl.saveUserProduct(product, user);
		List<UserProduct> userProducts = daoFitSystemImpl.getUserProduct(user, product);
		for (UserProduct userProduct : userProducts) {
			assertTrue(userProduct.getId_product() == product.getId());
			assertTrue(userProduct.getId_user() == user.getId());
			daoFitSystemImpl.deleteUserProductById(userProduct.getId_userPoducts());
		}
	}

	@Test
	public void testDeleteUserProductById() {
		User user = daoFitSystemImpl.getUserById(3);
		Product product = daoFitSystemImpl.getProduct(15);
		daoFitSystemImpl.saveUserProduct(product, user);
		List<UserProduct> userProducts = daoFitSystemImpl.getUserProduct(user, product);
		for (UserProduct userProduct : userProducts) {
			daoFitSystemImpl.deleteUserProductById(userProduct.getId_userPoducts());
		}
		List<UserProduct> userProductsTest = daoFitSystemImpl.getUserProduct(user, product);
		assertTrue(userProducts.size() != 0 && userProductsTest.size() == 0);
	}
}
