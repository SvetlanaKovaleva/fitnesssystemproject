### The FitnessSystem is web-application  for Fitness Centre ###
Project is based on Tomcat server and uses MySQL db so to start it :
1. run scripts from script.sql on your MySQL db ;
2. to run this project with Maven ;

All MySQL db settings are stored in  **src/main/webapp/WEB-INF/spring-database.xml**  and  **src/main/java/FitSysHib.cfg.xml**